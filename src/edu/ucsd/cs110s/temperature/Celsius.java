package edu.ucsd.cs110s.temperature;

public class Celsius extends Temperature {
	
	
	public Celsius(float t)
		{
			super(t);
		}
	public String toString()
		{
			// TODO: Complete this method
			return "" + this.getValue() + "C";
		}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Celsius temp = new Celsius(getValue());
		return temp;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Fahrenheit temp2 = new Fahrenheit(getValue()*(float)9/5 +32);
		return temp2;
	}	

}
